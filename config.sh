readonly DOCKER_IMAGE_NAME_INFO="watches-info"
readonly DOCKER_IMAGE_NAME_IMAGES="watches-images"
readonly DOCKER_IMAGE_TAG="1.0"
readonly DOCKER_IMAGE_MOUNT="/mnt/data"

readonly DOCKER_LOCAL_SOURCE_DIRECTORY_INFO="project_watches"
readonly DOCKER_LOCAL_SOURCE_DIRECTORY_IMAGES="watches_images"
readonly DOCKER_LOCAL_MOUNT_DIRECTORY_INFO="data_info"
readonly DOCKER_LOCAL_MOUNT_DIRECTORY_IMAGES="data_images"
