
from flask import Flask, abort, render_template
import requests

app = Flask(__name__)


@app.route('/image/v1/get/<string:sku>', methods=['GET'])
def hello(sku):
    image_url = f"https://s3-eu-west-1.amazonaws.com/cloudcomputing-2018/project1/images/{sku}.png"
    response = requests.get(image_url)
    if response.status_code == 200:
        return render_template("index.html", image_url=image_url)
    else:
        abort(404)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5001, debug=True)

