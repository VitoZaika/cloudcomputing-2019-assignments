#!/usr/bin/env bash
#
# Build esp8266 development container

readonly CURRENT_SCRIPT="$(basename -- ${BASH_SOURCE[0]})"
readonly CURRENT_DIRECTORY="$(cd "$(dirname -- "${BASH_SOURCE[0]}")" && pwd)"
readonly DOCKER_BINARY="$(command -v docker)"

source "${CURRENT_DIRECTORY}/config.sh"
# usage: usage [printer]
usage() {
  local printer="$(arg_or_default "$1" 'print_raw')"

  "${printer}" "usage: ${CURRENT_SCRIPT} [-h] [TAG]"
}

# usage: full_usage [printer]
full_usage() {
  local printer="$(arg_or_default "$1" 'print_raw')"

  usage "${printer}"
  "${printer}"
  "${printer}" 'Build script for docker'
  "${printer}"
  "${printer}" 'arguments:'
  "${printer}" '  -h                    show this help message and exit'
  "${printer}" '  TAG                   the tag of the image to build'
}


# usage: build_image [tag]
build_image() {
  # Generate image name
  local name_watches="${DOCKER_IMAGE_NAME_INFO}:${DOCKER_IMAGE_TAG}"
  local name_images="${DOCKER_IMAGE_NAME_IMAGES}:${DOCKER_IMAGE_TAG}"
  echo "building image info"

  # Run docker with the provided arguments
  docker build -t "${name_watches}" \
                  "${CURRENT_DIRECTORY}/${DOCKER_LOCAL_SOURCE_DIRECTORY_INFO}"

  echo "building image images"

  docker build -t "${name_images}" \
                  "${CURRENT_DIRECTORY}/${DOCKER_LOCAL_SOURCE_DIRECTORY_IMAGES}"


}

# usage: main [-h] [-d DATA_DIRECTORY] [-t TAG] [ARGS...]
main() {
  while getopts ':h' OPT; do
    case "${OPT}" in
      h)
        full_usage
        exit 0
        ;;
      ?)
        full_usage
        print
        error "invalid argument: ${OPTARG}"
        exit 1
        ;;
    esac
  done

  shift $((OPTIND - 1))

  build_image "$@"
}

if [[ "$0" == "${BASH_SOURCE[0]}" ]]; then
  main "$@"
fi
