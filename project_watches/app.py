from flask import Flask, request, Response, jsonify
import json
import server
from flask_cors import CORS
from flask_httpauth import HTTPBasicAuth
import _pickle as cPickle
import config

app = Flask(__name__)
app.config['BASIC_AUTH_USERNAME'] = 'cloud'
app.config['BASIC_AUTH_PASSWORD'] = 'computing'
CORS(app)
auth = HTTPBasicAuth()
currver = '/info/v1'


def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    raise TypeError


@app.route('/')
@auth.login_required
def test():
    return "Welcome to Flask!"


@app.route(currver + '/watch', methods=['POST'])
@auth.login_required
def add_to_list():
    req_data = request.get_json()
    _sku = req_data['sku']
    _type_watch = req_data['type']
    _status = req_data['status']
    _gender = req_data['gender']
    _year = req_data['year']
    _dial_material = req_data['dial_material']
    _dial_color = req_data['dial_color']
    _case_material = req_data['case_material']
    _case_form = req_data['case_form']
    _bracelet_material = req_data['bracelet_material']
    _movement = req_data['movement']
    res_data = server.add_to_list(_sku,_type_watch,_status,_gender,_year,_dial_material,_dial_color,_case_material,_case_form,_bracelet_material,_movement)
    if res_data is None:
        response = Response("{'error': 'Item not added - " + _sku + "'}", status=400 , mimetype='application/json')
        return response
    response = Response(json.dumps(res_data), mimetype='application/json')
    return response


@app.route(currver + '/watch/<string:sku>', methods=['GET', 'PUT', 'DELETE'])
@auth.login_required
def watch(sku):
    req_data = request.get_json()
    if request.method == "GET":
        res_data = server.get_watch(sku)
        response = Response(json.dumps(res_data), mimetype='application/json')
        return response
    elif request.method == "PUT":
        _sku = req_data['sku']
        _type_watch = req_data['type']
        _status = req_data['status']
        _gender = req_data['gender']
        _year = req_data['year']
        _dial_material = req_data['dial_material']
        _dial_color = req_data['dial_color']
        _case_material = req_data['case_material']
        _case_form = req_data['case_form']
        _bracelet_material = req_data['bracelet_material']
        _movement = req_data['movement']
        res_data = server.update_status(_sku,_type_watch,_status,_gender,_year,_dial_material,_dial_color,_case_material,_case_form,_bracelet_material,_movement)
        if res_data is None:
            response = Response("{'error': 'Error updating item - '" + _sku + "}", status=400,
                                mimetype='application/json')
            return response
        response = Response(json.dumps(res_data), mimetype='application/json')
        return response
    elif request.method == "DELETE":
        req_data = request.get_json()
        print("getting sku", sku)
        _sku = sku
        print("deleting item sku: ", _sku)
        res_data = server.delete_watch(_sku)
        if res_data is None:
            response = Response("{'error': 'Error deleting item - '" + sku + "}", status=400,
                                mimetype='application/json')
            return response
        response = Response(json.dumps(res_data), mimetype='application/json')
        return response
    elif request.method == "OPTIONS":
        req_data = request.get_json()
        res_data = server.get_watch(sku)
        response = Response(json.dumps(res_data), mimetype='application/json')
        return response

@app.route(currver + '/watch/complete-sku/<string:prefix>', methods=['GET'])
@auth.login_required
def watchbypref(prefix):
    res_data = server.get_by_pref(prefix)
    response = Response(json.dumps(res_data), mimetype='application/json')
    return response


@app.route(currver + '/allwatches', methods=['GET'])
@auth.login_required
def get_all_watches():
    res_data = server.get_all_watches()
    response = Response(json.dumps(res_data), mimetype='application/json')
    return response


class criteria_dictionary(dict):
    def __init__(self):
        self = dict()
    def add(self, key, value):
        self[key] = value


@app.route(currver + '/watch/find', methods=['GET', 'PUT', 'DELETE'])
@auth.login_required
def watchbyparam():
    dict_obj = criteria_dictionary()
    if request.method == "GET":
        _sku = ''
        _type_watch = ''
        _status = ''
        _gender = ''
        _year = ''
        _dial_material = ''
        _dial_color = ''
        _case_material = ''
        _case_form = ''
        _bracelet_material = ''
        _movement = ''
        if request.args.get('sku'):
            _sku = request.args.get('sku')
            if (len(_sku) != 0): dict_obj.add('sku', _sku)
        if request.args.get('type'):
            _type_watch = request.args.get('type')
            if (len(_type_watch) != 0): dict_obj.add('type', _type_watch)
        if request.args.get('status'):
            _status = request.args.get('status')
            if (len(_status) != 0): dict_obj.add('status', _status)
        if request.args.get('gender'):
            _gender = request.args.get('gender')
            if (len(_gender) != 0): dict_obj.add('gender', _gender)
        if request.args.get('year'):
            _year = request.args.get('year')
            if (len(_year) != 0): dict_obj.add('year', _year)
        if request.args.get('dial_material'):
            _dial_material = request.args.get('dial_material')
            if (len(_dial_material) != 0): dict_obj.add('dial_material', _dial_material)
        if request.args.get('dial_color'):
            _dial_color = request.args.get('dial_color')
            if (len(_dial_color) != 0): dict_obj.add('dial_color', _dial_color)
        if request.args.get('case_material'):
            _case_material = request.args.get('case_material')
            if (len(_case_material) != 0): dict_obj.add('case_material', _case_material)
        if request.args.get('case_form'):
            _case_form = request.args.get('case_form')
            if (len(_case_form) != 0): dict_obj.add('case_form', _case_form)
        if request.args.get('bracelet_material'):
            _bracelet_material = request.args.get('bracelet_material')
            if (len(_bracelet_material) != 0): dict_obj.add('bracelet_material', _bracelet_material)
        if request.args.get('movement'):
            _movement = request.args.get('movement')
            if (len(_movement) != 0): dict_obj.add('movement', _movement)
        res_data = server.get_by_criteria(_sku, _type_watch, _status, _gender, _year, _dial_material, _dial_color,
                                          _case_material, _case_form, _bracelet_material, _movement, dict_obj)
        response = Response(json.dumps(res_data), mimetype='application/json')
        return response


@auth.get_password
def get_password(username):
    if username == 'cloud':
        return 'computing'
    return None


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000, debug=True)