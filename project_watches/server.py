from builtins import Exception, len
import mysql.connector

NOTSTARTED = 'Not Started'
INPROGRESS = 'In Progress'
COMPLETED = 'Completed'

mydb = mysql.connector.connect(host='35.242.146.229', port=3306, user='watches',
                               password='watches', db='watches') #, auth_plugin='mysql_native_password'


def add_to_list(_sku, _type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
                _bracelet_material, _movement):
    try:
        c = mydb.cursor()
        c.execute(
            "INSERT INTO watches(sku,type,status,gender,year, dial_material, dial_color, case_material, case_form, bracelet_material, movement) VALUES(%s, %s, %s, %s,%s,%s,%s,%s,%s,%s,%s)",
            (_sku, _type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
             _bracelet_material, _movement))
        mydb.commit()
        return {"val": _sku, "status": NOTSTARTED}
    except Exception as e:
        print('Error: ', e)
        return None



def full_search(_sku,_type_watch,_status,_gender,_year,_dial_material,_dial_color,_case_material,_case_form,_bracelet_material,_movement):
    try:
        c = mydb.cursor()
        c.execute("SELECT * FROM watches INNER JOIN users ON users.curwatch = watches.id")
        mydb.commit()
        return {"val": _sku, "status": NOTSTARTED}
    except Exception as e:
        print('Error: ', e)
        return None
        
        
def get_watches_users(_sku,_type_watch,_status,_gender,_year,_dial_material,_dial_color,_case_material,_case_form,_bracelet_material,_movement):
    try:
        c = mydb.cursor()
        c.execute("SELECT * FROM watches LEFT JOIN users ON users.curwatch = watches.id")
        mydb.commit()
        return {"val": _sku, "status": NOTSTARTED}
    except Exception as e:
        print('Error: ', e)
        return None
        
        
def get_curwatches(_sku,_type_watch,_status,_gender,_year,_dial_material,_dial_color,_case_material,_case_form,_bracelet_material,_movement):
    try:
        c = mydb.cursor()
        c.execute("SELECT * FROM watches RIGHT JOIN users ON users.curwatch = watches.id")
        mydb.commit()
        return {"val": _sku, "status": NOTSTARTED}
    except Exception as e:
        print('Error: ', e)
        return None


def get_all_teachers():
    try:
        mycursor = mydb.cursor(prepared=True)
        mycursor.execute("SELECT * FROM teacher")
        myresult = mycursor.fetchall()
        return {"count": len(myresult), "items": myresult}
    except Exception as e:
        print('Error: ', e)
        return None


def get_all_watches():
    try:
        mycursor = mydb.cursor(prepared=True)
        mycursor.execute("SELECT * FROM watches GROUP BY sku HAVING year > 2006")
        myresult = mycursor.fetchall()
        return {"count": len(myresult), "items": myresult}
    except Exception as e:
        print('Error: ', e)
        return None


def get_watch(value):
    vals_list = []
    vals_list.append(value)
    try:
        mycursor = mydb.cursor(prepared=True)
        sql = "SELECT * FROM watches WHERE sku = %s "
        mycursor.execute(sql, tuple(vals_list))
        myresult = mycursor.fetchall()
        return {"sku": myresult[0][0], "type": sorted(myresult[0][1])[0], "status": sorted(myresult[0][2])[0],
                "gender": myresult[0][3], "year": myresult[0][4], "dial_material": myresult[0][5],
                "dial_color": myresult[0][6], "case_material": myresult[0][7], "case_form": myresult[0][8],
                "bracelet_material": myresult[0][9], "movement": myresult[0][10]}
    except Exception as e:
        print('Error: ', e)
        return None


def get_by_pref(value):
    try:
        val_list = []
        val_list.append("%" + value + "%")
        mycursor = mydb.cursor(prepared=True)
        sql = "SELECT * FROM watches WHERE sku LIKE %s"
        mycursor.execute(sql, tuple(val_list))
        myresult = mycursor.fetchall()
        return {"count": len(myresult), "items": myresult}
    except Exception as e:
        print('Error: ', e)
        return None


def generate_sql(dictionary):
    if len(dictionary) == 1:
        if len(dictionary.get('sku')) != 0:
            sql = 'SELECT * FROM watches WHERE ' + list(dictionary.items())[0][0] + ' LIKE %s'
            return sql
        else:
            sql = 'SELECT * FROM watches WHERE ' + list(dictionary.items())[0][0] + '=%s'
            return sql
    else:
        sql = 'SELECT * FROM watches WHERE'
        n = 0
        for i in dictionary.items():
            if n == 0:
                if len(dictionary.get('sku')) != 0:
                    sql = sql + " " + i[0] + ' LIKE %s'
                else:
                    sql = sql + " " + i[0] + '=%s'
            else:
                if n == (len(dictionary) - 1):
                    sql = sql + " AND " + i[0] + '=%s'
                else:
                    sql = sql + " AND " + i[0] + '=%s'
            n = n + 1
        return (sql)


def get_by_criteria(_sku, _type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
                    _bracelet_material, _movement, dict_obj):
    sql = generate_sql(dict_obj)
    values = []
    list_vals = []
    for i in dict_obj.items():
        values.append(i[0])
    for i in values:
        if i == "sku":
            list_vals.append("%" + dict_obj.get(i) + "%")
        else:
            list_vals.append(dict_obj.get(i))
    try:
        mycursor = mydb.cursor(prepared=True)
        mycursor.execute(sql, tuple(list_vals))
        myresult = mycursor.fetchall()
        return myresult
    except Exception as e:
        print('Error: ', e)
        return None


def delete_watch(sku):
    try:
        val_list = []
        val_list.append(sku)
        conn = mydb
        c = conn.cursor(prepared=True)
        sql = "DELETE FROM watches WHERE sku = %s"
        c.execute(sql, tuple(val_list))
        conn.commit()
        return {'sku': sku}
    except Exception as e:
        print('Error: ', e)
        return None


def update_status(_sku, _type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
                  _bracelet_material, _movement):
    try:
        conn = mydb
        c = conn.cursor()
        c.execute(
            "UPDATE watches SET type=%s,status=%s,gender=%s,year=%s,dial_material=%s,dial_color=%s,case_material=%s,case_form=%s,bracelet_material=%s,movement=%s WHERE sku=%s",
            (_type_watch, _status, _gender, _year, _dial_material, _dial_color, _case_material, _case_form,
             _bracelet_material, _movement, _sku))
        conn.commit()
        return {'sku': _sku}
    except Exception as e:
        print('Error: ', e)
        return 'Failure'
